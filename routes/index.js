var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send(JSON.stringify({ title: 'Express' }));
});

/**
 * respond what receive on post
 */
router.post('/test', function(req, res, next) {
  res.send(JSON.stringify(req.body));
});

module.exports = router;
